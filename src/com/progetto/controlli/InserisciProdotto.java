package com.progetto.controlli;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.progetto.classiOggetti.Prodotto;

/**
 * Servlet implementation class InserisciProdotto
 */
@WebServlet("/InserisciProdotto")
public class InserisciProdotto extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("index.html");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession sessione = request.getSession();
		
		if(sessione.getAttribute("ruolo").equals("admin")) {
			String nomeProdotto = request.getParameter("input_nome");
			String codiceProdotto = request.getParameter("input_codice");
			float prezzoProdotto = Float.parseFloat(request.getParameter("input_prezzo"));
			int quantitaProdotto = Integer.parseInt(request.getParameter("input_quantita"));
			Prodotto temp = new Prodotto(nomeProdotto,codiceProdotto,prezzoProdotto,quantitaProdotto);
			
			GestisciProdotti gp = new GestisciProdotti();
			 
			try {
				if(!gp.esisteProdotto(codiceProdotto)) {
				gp.inserisciProdotto(temp);
				response.sendRedirect("AdminPage.jsp");
				}else {
					response.sendRedirect("index.html");
		
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}else {
			doGet(request,response);
		}
		
		
	}

}
