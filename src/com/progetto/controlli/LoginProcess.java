package com.progetto.controlli;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.jdbc.PreparedStatement;
import com.progetto.classiOggetti.Admin;
import com.progetto.classiOggetti.Connettore;
import com.progetto.classiOggetti.Prodotto;
import com.progetto.classiOggetti.RuoloUtenti;
import com.progetto.classiOggetti.Utente;

/**
 * Servlet implementation class LoginProcess
 */
@WebServlet("/LoginProcess")
public class LoginProcess extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.sendRedirect("index.html");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String varEmail = request.getParameter("input_email");
		String varPass = request.getParameter("input_password");
		String varRole = request.getParameter("select_role");
		
		HttpSession sessione = request.getSession();
		if(!varEmail.isBlank() && !varPass.isBlank() && !varRole.isBlank()) {
		
			if(varRole.equalsIgnoreCase("utente")) {
				Utente ut = new Utente();
				ut.setEmail(varEmail);
				ut.setPass(varPass);
				ut.setRuolo(varRole);
				try {
					if(checkUser(ut)) {
						sessione.setAttribute("ruolo", "utente");
						response.sendRedirect("consulta");
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}else if(varRole.equalsIgnoreCase("admin")) {
				Utente ut = new Utente(); //dovevo mettere admin
				ut.setEmail(varEmail);
				ut.setPass(varPass);
				ut.setRuolo(varRole);
				try {
					if(checkUser(ut)) {
						sessione.setAttribute("ruolo","admin");
						response.sendRedirect("AdminPage.jsp");
						
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		}else {
			doGet(request, response);
		}
		
	}

	private boolean checkUser(Utente ut) throws SQLException {
		Connection conn = Connettore.getInstance().getConnection();
		
		String query = "SELECT utenteId, nome,email,pass,ruolo from utente where email = ? and pass= MD5(?) AND ruolo = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		
		ps.setString(1, ut.getEmail());
		ps.setString(2, ut.getPass());
		ps.setString(3, ut.getRuolo());
		ResultSet risultato = ps.executeQuery();
		
		if(risultato.next()) {
			return true;
		}else {
			return false;
			
		}
		
	}

}
