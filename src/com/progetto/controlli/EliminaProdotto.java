package com.progetto.controlli;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class EliminaProdotto
 */
@WebServlet("/EliminaProdotto")
public class EliminaProdotto extends HttpServlet {
	private static final long serialVersionUID = 1L;
  

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("index.html");
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int idProdotto;
		try {
			idProdotto = Integer.parseInt(request.getParameter("input_id"));
			
		} catch (Exception e) {
			System.out.println("il numero che hai inserito non e valido.");
			return;
		}
		
		GestisciProdotti gp = new GestisciProdotti();
		try {
			gp.eliminaProdottobyid(idProdotto);
		} catch (SQLException e) {
			System.out.println("errore prodotto non eliminato");
			e.printStackTrace();
		}
		response.sendRedirect("AdminPage.jsp");
		
	}

}
