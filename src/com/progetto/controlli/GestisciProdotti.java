package com.progetto.controlli;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import com.progetto.classiOggetti.Connettore;
import com.progetto.classiOggetti.Prodotto;

public class GestisciProdotti {
	private ArrayList <Prodotto> elencoProd = new ArrayList<Prodotto>();
	
	
	public ArrayList<Prodotto> findAll() throws SQLException {
		Connection conn = Connettore.getInstance().getConnection();
		
		String query = "SELECT prodottoid,nome,codice,prezzo,quantita from prodotto";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		
		ResultSet risultato = ps.executeQuery();
		
		while(risultato.next()) {
			Prodotto temp = new Prodotto();
			temp.setProdottoId(risultato.getInt(1));
			temp.setNome(risultato.getString(2));
			temp.setCodice(risultato.getString(3));
			temp.setPrezzo(risultato.getFloat(4));
			temp.setQuantita(risultato.getInt(5));
			elencoProd.add(temp);
		}
		return elencoProd;
		
		
	}
	
	public boolean eliminaProdottobyid(int varId) throws SQLException {
		findAll();
		
		Connection conn = Connettore.getInstance().getConnection();

        String query_delete = "DELETE FROM prodotto WHERE prodottoId = ?";
        PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query_delete);
        ps.setInt(1, varId);                                       

        int risultato_operazione = ps.executeUpdate();
        if(risultato_operazione > 0) {
        	eliminaProdottoInLocale(varId);
            
            return true;
        }
        else {
        	return false;
        	
        }
		
		
		
	}

	private boolean eliminaProdottoInLocale(int varId) {
		for(int i=0;i<elencoProd.size();i++) {
			Prodotto temp = elencoProd.get(i);
			if(temp.getProdottoId()==varId) {
				elencoProd.remove(i);
				return true;
			}
			
		}
		return false;
		
	}

	public boolean inserisciProdotto(Prodotto objProdotto) throws SQLException {
		findAll();
		Connection conn = Connettore.getInstance().getConnection();
		
		String query_inserimento = "INSERT INTO Prodotto(Nome, codice, prezzo, quantita) VALUE (?, ?, ?, ?)";
		PreparedStatement ps = (PreparedStatement)conn.prepareStatement(query_inserimento, Statement.RETURN_GENERATED_KEYS);
		
		ps.setString(1, objProdotto.getNome());
		ps.setString(2, objProdotto.getCodice());
		ps.setFloat(3, objProdotto.getPrezzo());
		ps.setInt(4, objProdotto.getQuantita());
		
		ps.executeUpdate();								//Eseguo l'inserimento
		ResultSet risultato = ps.getGeneratedKeys();	//Prendo il risultato che  l'indice (AI) della riga appena creata
		risultato.next();
		
		int id_risultante = risultato.getInt(1);
		objProdotto.setProdottoId(id_risultante);
		if(id_risultante>0) {
			elencoProd.add(objProdotto);
			return true;
		}
		return false;
		
		
		
	}

	public boolean esisteProdotto(String codiceProdotto) throws SQLException {
		Connection conn = Connettore.getInstance().getConnection();
		
		String query = "SELECT prodottoid,nome,codice from prodotto WHERE codice = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setString(1, codiceProdotto);
		ResultSet risultato = ps.executeQuery();
		
		if(risultato.next()) {
			return true;
		}else {
			return false;
		}
	}




}
