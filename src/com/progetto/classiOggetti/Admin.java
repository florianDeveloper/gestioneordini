package com.progetto.classiOggetti;

public class Admin extends RuoloUtenti{

	private String ruolo = "admin";
	
	public Admin () {
		
	}
	public Admin(String varNome,String varEmail,String varPass) {
		super.nome=varNome;
		super.email=varEmail;
		super.pass=varPass;
	}

	public String getRuolo() {
		return ruolo;
	}

	public void setRuolo(String ruolo) {
		this.ruolo = ruolo;
	}

}
