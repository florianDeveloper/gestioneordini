package com.progetto.classiOggetti;

public class Prodotto {
	private int prodottoId;
	private String nome;
	private String codice;
	private float prezzo;
	private int quantita;
	
	
	public Prodotto(){
		
	}


	public Prodotto(String nome, String codice, float prezzo, int quantita) {
		this.nome = nome;
		this.codice = codice;
		this.prezzo = prezzo;
		this.quantita = quantita;
	}


	public int getProdottoId() {
		return prodottoId;
	}


	public void setProdottoId(int prodottoId) {
		this.prodottoId = prodottoId;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getCodice() {
		return codice;
	}


	public void setCodice(String codice) {
		this.codice = codice;
	}


	public float getPrezzo() {
		return prezzo;
	}


	public void setPrezzo(float prezzo) {
		this.prezzo = prezzo;
	}


	public int getQuantita() {
		return quantita;
	}


	public void setQuantita(int quantita) {
		this.quantita = quantita;
	}
	

}
