
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="com.progetto.classiOggetti.Prodotto"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.progetto.controlli.GestisciProdotti"%>
<!DOCTYPE html>
<html>
<head>
<title>gestione prodotti</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col">
				<table class="table">
					<thead>
						<tr>
							<th>Id</th>
							<th>Nome</th>
							<th>codice</th>
							<th>prezzo ($)</th>
							<th>quantita(pezzi)</th>
							
							
						</tr>
					</thead>
					<tbody>
						
						<%
						HttpSession sessione;
						String ruolo = "utente";
						try{
							sessione = request.getSession();
							ruolo = (String)sessione.getAttribute("ruolo");
						}catch(Exception e){
							out.print("non sei un admin");
						}
						if(ruolo.equals("admin")){
							
							ArrayList<Prodotto> elencoP = new ArrayList<Prodotto>();
							GestisciProdotti gp = new GestisciProdotti();
						
							elencoP = gp.findAll();
	
							String riga_risultante = "";
	
							for (int i = 0; i < elencoP.size(); i++) {
								Prodotto temp = elencoP.get(i);
	
								riga_risultante += "<tr>";
								riga_risultante += "<td>" + temp.getProdottoId()+ "</td>";
								riga_risultante += "<td>" + temp.getNome() + "</td>";
								riga_risultante += "<td>" + temp.getCodice() + "</td>";
								riga_risultante += "<td>" + temp.getPrezzo() +" $"+ "</td>";
								riga_risultante += "<td>" + temp.getQuantita()+ "</td>";
								riga_risultante += "</tr>";
							}
	
							out.print(riga_risultante);
						}else{
							out.print("erore non sei un admin per visualizzare la pagina");
						}
							%>
							
						
							
						

					</tbody>
				</table>
			</div>
			
		</div>
		<div class="row">
			<div class = "col md-4">
				<button type="button" class="btn btn-secondary" onclick="history.go(-1)">
				Torna Indietro</button>
			
			</div>
			<div class="col md-4">
				<form action ="" method="GET">
					<button type="button" class="btn btn-success" onclick="window.location.href='InserimentoProdotto.jsp'">Inserisci Prodotto</button>
				</form>
			
			</div>
			<div class = "col md-4">
				<form action ="EliminaProdotto" method="POST">
					<!-- <label for="input_id">id da eliminare:</label> -->
					<input type="text" placeholder ="id da eliminare" class="form-control" name="input_id" id="input_id" />
					<button type="submit" class="btn btn-danger mt-2">Elimina Prodotto</button>
				</form>
			</div>
			
			
			
		</div>
	</div>


	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
</body>
</html>