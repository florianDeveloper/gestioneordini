<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>
</head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<body>

    <div class="container">
        <form name="login" action="LoginProcess" method="POST">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6 text-center">
                    <h1>login</h1>
                    <div class="form-group">
                        <label for="input_email">email:</label>
                        <input type="email" class="form-control" name="input_email" id="input_email" />
                    </div>

                    <div class="form-group">
                        <label for="input_cognome">password:</label>
                        <input type="password" class="form-control" name="input_password" id="input_password" />
                    </div>
                    <div class="form-group">
						<label for="select_role">ruolo: </label>
						<select class="form-control" name="select_role" id="select_role">
							<option value="admin">admin</option>
							<option value="utente" selected>utente</option>
						</select>
					</div>

                    <button type="submit" class="btn btn-primary btn-block">login</button>
                </div>
                <div class="col-md-3"></div>
            </div>
        </form>
    </div>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>